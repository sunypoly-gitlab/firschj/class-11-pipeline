# GitLab Pipeline Intro

1. Create a pipeline with one stage that just echos a statement
1. Update this pipeline to use a variable
1. Add a stage
1. Add a condition to the second stage
1. Make the second stage export an artifact
